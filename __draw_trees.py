import sys
sys.path.append("cognate_evolution_models")
import cognate_evolution_models.small_tools.io_tools as io
from cognate_evolution_models.trees import tree_basics
from cognate_evolution_models.trees import tree_construct
import polynesian_trees as pt
import polynesian_data_parameters as pdp

tree_1 = pt.pt_1

# tree 1 with all leaves having same distance from root
nodes = {"PPn", "PTo", "PNPn", "PEPn", "PCEPn",
         "niu", "ton", "fud", "pkp", "mnv", "tkp", "wls", "smo", "fut",
         "rap", "haw", "mrv", "mri", "pmt", "tah", "mrq", "rar"}
edges = {("PPn", "PTo"): 1, ("PPn", "PNPn"): 1, ("PNPn", "PEPn"): 1, ("PEPn","PCEPn"): 1,
         ("PTo", "niu"): 3, ("PTo", "ton"): 3, ("PNPn", "fud"): 3, ("PNPn", "pkp"): 3, ("PNPn", "mnv"): 3,
         ("PNPn", "tkp"): 3, ("PNPn", "wls"): 3, ("PNPn", "smo"): 3, ("PNPn", "fut"): 3,
         ("PEPn", "rap"): 2, ("PCEPn", "haw"): 1, ("PCEPn", "mrv"): 1, ("PCEPn", "mri"): 1,
         ("PCEPn", "pmt"): 1, ("PCEPn", "tah"): 1, ("PCEPn", "mrq"): 1, ("PCEPn", "rar"): 1}
root = "PPn"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

tree_1_same_dist = [nodes, edges, root, leaves, internals, parents, children]

nodes_to_be_renamed = nodes.difference({"PPn", "PTo", "PNPn", "PEPn", "PCEPn"})
new_names = dict()
for node in nodes_to_be_renamed:
    name_without_code = pdp.lang_names[pdp.lang_matches_1[node]]
    new_names[node] = name_without_code + " [" + node +"]"
# shorten Marquesan name
new_names["mrq"] = "Marquesan (Nukuhiva) [mrq]"
tree_1_renamed = tree_construct.rename_nodes_to(tree_1_same_dist, new_names)

io.draw_tree(tree_1_renamed)