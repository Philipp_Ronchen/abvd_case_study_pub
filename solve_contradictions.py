import sys
sys.path.append("cognate_evolution_models")
import copy
from cognate_evolution_models.trees import tree_tools
from cognate_evolution_models.small_tools import dict_tools
from cognate_evolution_models.lh_calculation.lh_multistate_recursive import extend_data
from cognate_evolution_models.data_transform import multistate_data_modify as mdm

# Gives all data sets that are consistent and that can be reached from the original data set by splitting a minimal
# number of cognate classes.
# Data should be non-extended one meaning data.
def get_closest_consistent_data_sets(data, tree):
    nodes = tree[0]
    internals = tree[4]
    parents = tree[5]
    children = tree[6]
    levels = tree_tools.get_downward_levels(tree)
    levels = map(lambda x: frozenset(x.intersection(internals)), levels)
    levels = [level for level in levels if level]
    # print("levels")
    # print(levels)
    num_levels = len(levels)
    split_data_sets = {i:list() for i in range(num_levels)}
    for i in range(num_levels):
        # print("level id")
        # print(i)
        if i == 0:
            prev_level_split_data_sets = [copy.deepcopy(data)]
        else:
            prev_level_split_data_sets = split_data_sets[i-1]
        for data_set in prev_level_split_data_sets:
            # print("data set to be split")
            # print(data_set)
            extension = extend_data(tree, data_set)
            if extension[0]:
                # print("data set has extension!")
                split_data_sets[i].append(copy.deepcopy(data_set))
            else:
                # print("data set has no consistent extension!")
                extended_data_set = extension[1]
                # print("non-consistent extension:")
                # print(extended_data_set)
                possibilities = dict()
                for node in levels[i]:
                    if node in extended_data_set and len(extended_data_set[node]) >= 2:
                        if node in data_set:
                            # if node is a data node but inconsistent, all other determined values will have to be
                            # dealt with
                            values_to_be_separated = copy.deepcopy(extended_data_set[node])
                            values_to_be_separated.remove(data_set[node])
                            possibilities[node] = [values_to_be_separated]
                            # print("values to be separated")
                            # print(values_to_be_separated)
                        else:
                            # if node is not a data node, one determined values can be kept, but all other values
                            # nead to be dealt with
                            possibilities[node] = [{value for value in extended_data_set[node] if not value == kept_value}
                                                   for kept_value in extended_data_set[node]]
                combinations = list(dict_tools.dict_product(possibilities))
                # print("combinations")
                # print(combinations)
                if combinations == [{}]:
                    # print("no combination")
                    upper_level_data_set = copy.deepcopy(data_set)
                    split_data_sets[i].append(upper_level_data_set)
                else:
                    for comb in combinations:
                        # print("current combination")
                        # print(comb)
                        comb_data_set = copy.deepcopy(data_set)
                        used_values = set(comb_data_set.values())
                        for node in comb.keys():
                            # print("current node")
                            # print(node)
                            # print("hi")
                            for value in comb[node]:
                                value_neighbours = [node for node in children[node]
                                                  if node in extended_data_set and value in extended_data_set[node]]
                                if node in parents and parents[node] in extended_data_set and \
                                        value in extended_data_set[parents[node]]:
                                    value_neighbours.append(parents[node])
                                for neighbour in value_neighbours[1:]:
                                    if neighbour in children[node]:
                                        affected_nodes = tree_tools.get_subtree_nodes(tree, neighbour)
                                    else:
                                        affected_nodes = nodes.difference(tree_tools.get_subtree_nodes(tree, node))
                                    min_unused_value = mdm.get_min_unused_value(used_values)
                                    comb_data_set = mdm.replace_data_value(min_unused_value, value, affected_nodes, comb_data_set)
                                    used_values.add(min_unused_value)
                        split_data_sets[i].append(comb_data_set)
                        # print("comb data set added:")
                        # print(comb_data_set)
                        # print("split data sets:")
                        # print(split_data_sets)
    # print(split_data_sets)
    return split_data_sets[num_levels - 1]


