import sys
sys.path.append("cognate_evolution_models")
import pickle
import csv
from cognate_evolution_models.small_tools import math_tools

# extracts the maximum likelihoods per tree age given the different alternative data sets, for different truncation
# levels, that is a different number of meanings excluded. Writes both a python dictionary and a csv.


ages = list(range(600,3501,100))

truncation_levels = [0, 5, 10, 15, 20, 30, 40, 50]

for param in ["by_tree_age", "by_epn_age"]:
    for truncation_level in truncation_levels:
        if param == "by_tree_age":
            csv_filepath = "max_lhs_summaries/max_lhs_summary_by_ppn_age_trunc_" + str(truncation_level) + ".csv"
        else:
            csv_filepath = "max_lhs_summaries/max_lhs_summary_by_pepn_age_trunc_" + str(truncation_level) + ".csv"
        with open(csv_filepath, 'w', newline='') as csvfile:
            fieldnames = ["age", "max_lh_highest", "max_lh_lowest",
                          "tree_max_lh_highest", "tree_max_lh_lowest",
                          "meanings_excluded_highest", "meanings_excluded_lowest",
                          "num_times_highest_is_sep", "num_times_highest_is_eq",
                          "num_times_lowest_is_sep", "num_times_lowest_is_eq"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            for age in ages:
                if param == "by_tree_age":
                    a_file = open("lhs_by_tree_age/ppn_" + str(age) + "_lhs", "rb")
                else:
                    a_file = open("lhs_by_epn_age/pepn_" + str(age) + "_lhs", "rb")
                lhs = pickle.load(a_file)
                a_file.close()

                some_tree = list(lhs.keys())[0]
                meanings = lhs[some_tree].keys()

                # per tree and meaning, stores the highest lh for this meaning among the data sets
                highest_lhs = dict()
                # per tree and meaning, stores the lowest lh for this meaning among the data sets
                lowest_lhs = dict()
                # per tree and meaning, stores whether highest lh is given by sep or eq data set
                highest_lh_is_sep = dict()
                # per tree and meaning, stores whether lowest lh is given by sep or eq data set
                lowest_lh_is_sep = dict()

                # per tree, for the given truncation level, stores the sum of logs of the highest lhs, truncating the
                # smallest highest lhs
                highest_lh_log_sums = dict()
                # per tree, for the given truncation level, stores the sum of logs of the lowest lhs, truncating the
                # smallest lowest lhs
                lowest_lh_log_sums = dict()
                # per tree, for the given truncation level, stores which meanings are excluded when maximising the product
                # of the highest likelihoods meaningswise
                meanings_excluded_max_highest_lhs = dict()
                # per tree, for the given truncation level, stores which meanings are excluded when maximising the product
                # of the lowest likelihoods meaningswise
                meanings_excluded_max_lowest_lhs = dict()


                for tree in lhs.keys():
                    highest_lhs[tree] = dict()
                    lowest_lhs[tree] = dict()
                    highest_lh_is_sep[tree] = dict()
                    lowest_lh_is_sep[tree] = dict()

                    for m in meanings:
                        highest_sep_lh = max(lhs[tree][m][0])
                        highest_eq_lh = max(lhs[tree][m][1])
                        lowest_sep_lh = min(lhs[tree][m][0])
                        lowest_eq_lh = min(lhs[tree][m][1])
                        if highest_sep_lh >= highest_eq_lh:
                            highest_lh_is_sep[tree][m] = True
                        else:
                            highest_lh_is_sep[tree][m] = False
                        if lowest_sep_lh <= lowest_eq_lh:
                            lowest_lh_is_sep[tree][m] = True
                        else:
                            lowest_lh_is_sep[tree][m] = False
                        highest_lh = max(highest_sep_lh, highest_eq_lh)
                        lowest_lh = min(lowest_sep_lh, lowest_eq_lh)

                        highest_lhs[tree][m] = highest_lh
                        lowest_lhs[tree][m] = lowest_lh

                    highest_lh_items = list(highest_lhs[tree].items())
                    lowest_lh_items = list(lowest_lhs[tree].items())
                    highest_lh_items.sort(key=lambda x: x[1], reverse=True)
                    lowest_lh_items.sort(key=lambda x: x[1], reverse=True)

                    if truncation_level == 0:
                        trunc_highest_lh_items = highest_lh_items
                        trunc_lowest_lh_items = lowest_lh_items
                    else:
                        trunc_highest_lh_items = highest_lh_items[:-truncation_level]
                        trunc_lowest_lh_items = lowest_lh_items[:-truncation_level]
                    meanings_left_highest_lhs = {item[0] for item in trunc_highest_lh_items}
                    meanings_left_lowest_lhs = [item[0] for item in trunc_lowest_lh_items]
                    meanings_excluded_max_highest_lh = {m for m in meanings if m not in meanings_left_highest_lhs}
                    meanings_excluded_max_lowest_lh = {m for m in meanings if m not in meanings_left_lowest_lhs}

                    trunc_highest_log_lhs = [math_tools.inf_log(item[1]) for item in trunc_highest_lh_items]
                    trunc_lowest_log_lhs = [math_tools.inf_log(item[1]) for item in trunc_lowest_lh_items]

                    highest_lh_log_sum = sum(trunc_highest_log_lhs)
                    lowest_lh_log_sum = sum(trunc_lowest_log_lhs)

                    meanings_excluded_max_highest_lhs[tree] = meanings_excluded_max_highest_lh
                    meanings_excluded_max_lowest_lhs[tree] = meanings_excluded_max_lowest_lh
                    highest_lh_log_sums[tree] = highest_lh_log_sum
                    lowest_lh_log_sums[tree] = lowest_lh_log_sum

                tree_lh_items_highest = list(highest_lh_log_sums.items())
                tree_lh_items_lowest = list(lowest_lh_log_sums.items())
                tree_lh_items_highest.sort(key=lambda x: x[1], reverse=True)
                tree_lh_items_lowest.sort(key=lambda x: x[1], reverse=True)

                (tree_max_highest, max_lh_highest) = tree_lh_items_highest[0]
                (tree_max_lowest, max_lh_lowest) = tree_lh_items_lowest[0]

                meanings_excluded_max_highest = meanings_excluded_max_highest_lhs[tree_max_highest]
                meanings_excluded_max_lowest = meanings_excluded_max_lowest_lhs[tree_max_lowest]

                num_times_highest_is_sep = len([m for m in meanings if m not in meanings_excluded_max_highest
                                                and highest_lh_is_sep[tree_max_highest][m]])
                num_times_highest_is_eq = len([m for m in meanings if m not in meanings_excluded_max_highest
                                                and not highest_lh_is_sep[tree_max_highest][m]])
                num_times_lowest_is_sep = len([m for m in meanings if m not in meanings_excluded_max_highest
                                                and lowest_lh_is_sep[tree_max_lowest][m]])
                num_times_lowest_is_eq = len([m for m in meanings if m not in meanings_excluded_max_highest
                                               and not lowest_lh_is_sep[tree_max_lowest][m]])

                writer.writerow({"age": str(age),
                                 "max_lh_highest": str(max_lh_highest),
                                 "max_lh_lowest": str(max_lh_lowest),
                                 "tree_max_lh_highest": str(tree_max_highest),
                                 "tree_max_lh_lowest": str(tree_max_lowest),
                                 "meanings_excluded_highest": str(meanings_excluded_max_highest),
                                 "meanings_excluded_lowest": str(meanings_excluded_max_lowest),
                                 "num_times_highest_is_sep": str(num_times_highest_is_sep),
                                 "num_times_highest_is_eq": str(num_times_highest_is_eq),
                                 "num_times_lowest_is_sep": str(num_times_lowest_is_sep),
                                 "num_times_lowest_is_eq": str(num_times_lowest_is_eq)
                                 })
