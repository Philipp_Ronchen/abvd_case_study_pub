import pickle

def get_tree_lh(node_ages):
    age = node_ages[0]
    a_file = open("../nobackup/lhs_by_tree_age/ppn_" + str(age) + "_lhs", "rb")
    lhs = pickle.load(a_file)
    a_file.close()
    tree_lh = lhs[node_ages]
    return tree_lh