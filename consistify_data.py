#import sys
#sys.path.append("cognate_evolution_models")
#from cognate_evolution_models.small_tools.io_tools import draw_tree
import solve_contradictions as sc
import polynesian_trees as pt
import pickle

tree = pt.pt_1

# read data
a_file = open("data_pickled_python/t1_separated_data.pkl", "rb")
separated_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_equalized_data.pkl", "rb")
equalized_data = pickle.load(a_file)
a_file.close()

cons_separated_data_sets = dict()
cons_equalized_data_sets = dict()

meanings = separated_data.keys()
for m in meanings:
    cons_separated_data_sets[m] = sc.get_closest_consistent_data_sets(separated_data[m], tree)
    cons_equalized_data_sets[m] = sc.get_closest_consistent_data_sets(equalized_data[m], tree)


a_file = open("data_pickled_python/t1_cons_separated_data.pkl", "wb")
pickle.dump(cons_separated_data_sets, a_file)
a_file.close()

a_file = open("data_pickled_python/t1_cons_equalized_data.pkl", "wb")
pickle.dump(cons_equalized_data_sets, a_file)
a_file.close()


