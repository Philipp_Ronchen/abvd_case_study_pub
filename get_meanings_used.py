import pickle

a_file = open("data_pickled_python/rates_dict.pkl", "rb")
rates_dict = pickle.load(a_file)
a_file.close()

meanings_used = set(rates_dict.keys())

a_file = open("data_pickled_python/meanings_used.pkl", "wb")
pickle.dump(meanings_used, a_file)
a_file.close()