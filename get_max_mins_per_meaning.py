import csv
import pickle

# Writes maximum and minimum likelihood values per meaning to a csv file. Used to check whether there are obvious
# floating point rounding errors or whether there occured underflow/overflow. Does not store trees where max and min
# occured.
def ex():
    num_chunks = 20

    with open("analysis_statistics/max_min_per_meaning.csv", 'w', newline='') as csvfile:
        fieldnames = ["meaning_code", "min_sep", "max_sep", "min_eq", "max_eq"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for i in range(num_chunks):
            a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_separated_data.pkl", "rb")
            sep_chunk = pickle.load(a_file)
            a_file.close()

            a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_equalized_data.pkl", "rb")
            eq_chunk = pickle.load(a_file)
            a_file.close()

            some_tree = list(sep_chunk.keys())[0]
            meanings = sep_chunk[some_tree].keys()

            for m in sorted(meanings):
                m_values_sep = [value for tree in sep_chunk.keys() for value in sep_chunk[tree][m]]
                m_values_eq = [value for tree in eq_chunk.keys() for value in eq_chunk[tree][m]]
                writer.writerow({"meaning_code": str(m),
                                "min_sep": str(min(m_values_sep)),
                                 "max_sep": str(max(m_values_sep)),
                                 "min_eq": str(min(m_values_eq)),
                                 "max_eq": str(max(m_values_eq))
                                 })
