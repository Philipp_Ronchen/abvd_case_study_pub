import pickle
import polynesian_trees as pt
import csv

# read data
a_file = open("data_pickled_python/t1_cons_separated_data.pkl", "rb")
cons_separated_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_cons_equalized_data.pkl", "rb")
cons_equalized_data = pickle.load(a_file)
a_file.close()

meanings = cons_separated_data.keys()
num_meanings = len(set(meanings))

leaves = pt.pt_1[3]

treename = "tree_1"

total_num_classes_separated = 0
total_num_classes_equalized = 0
with open("data_summaries/" + treename + "_cons_data_summaries.csv", 'w', newline='') as csvfile:
    fieldnames = ["meaning_code", "num_cons_separated_data_sets", "num_cons_equalized_data_sets",
                  "min_cons_separated_num_cog_classes", "max_cons_separated_num_cog_classes",
                  "min_cons_equalized_num_cog_classes", "max_cons_equalized_num_cog_classes"]#,
                  #"sep_num_internals_contradictory", "eq_num_internals_contradictory"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    total_num_sep_data_sets = 0
    total_num_eq_data_sets = 0
    for m in meanings:
        one_meaning_separated_data_sets = cons_separated_data[m]
        one_meaning_equalized_data_sets = cons_equalized_data[m]
        num_separated_data_sets = len(one_meaning_separated_data_sets)
        num_equalized_data_sets = len(one_meaning_equalized_data_sets)
        total_num_sep_data_sets += num_separated_data_sets
        total_num_eq_data_sets += num_equalized_data_sets
        nums_cog_classes_separated = list(map(lambda x: len(set(x.values())), one_meaning_separated_data_sets))
        nums_cog_classes_equalized = list(map(lambda x: len(set(x.values())), one_meaning_equalized_data_sets))
        min_cog_classes_separated = min(nums_cog_classes_separated)
        max_cog_classes_separated = max(nums_cog_classes_separated)
        min_cog_classes_equalized = min(nums_cog_classes_equalized)
        max_cog_classes_equalized = max(nums_cog_classes_equalized)
        writer.writerow({"meaning_code": str(m),
                        "num_cons_separated_data_sets": str(num_separated_data_sets),
                        "num_cons_equalized_data_sets": str(num_equalized_data_sets),
                        "min_cons_separated_num_cog_classes": str(min_cog_classes_separated),
                        "max_cons_separated_num_cog_classes": str(max_cog_classes_separated),
                        "min_cons_equalized_num_cog_classes": str(min_cog_classes_equalized),
                        "max_cons_equalized_num_cog_classes": str(max_cog_classes_equalized)
                        })


print("number of meanings")
print(num_meanings)
print("total number separated data sets")
print(total_num_sep_data_sets)
print("total number equalized data sets")
print(total_num_eq_data_sets)

