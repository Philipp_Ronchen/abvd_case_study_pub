import pickle
import polynesian_trees as pt
import csv

# read data
a_file = open("t1_extended_data.pkl", "rb")
extended_data = pickle.load(a_file)
a_file.close()

a_file = open("t1_equalized_data.pkl", "rb")
equalized_data = pickle.load(a_file)
a_file.close()

meanings = extended_data.keys()

leaves = pt.pt_1[3]

treename = "tree_1"

total_num_classes_extended = 0
total_num_classes_equalized = 0
total_num_leaf_classes_extended = 0
total_num_leaf_classes_equalized = 0
with open("data_summaries/" + treename + "_curated_data_summaries.csv", 'w', newline='') as csvfile:
    fieldnames = ["meaning_code", "max_num_cog_classes", "min_num_cog_classes"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for m in meanings:
        one_meaning_data = extended_data[m]
        one_meaning_equalized_data = equalized_data[m]
        one_meaning_leaf_data = {node:one_meaning_data[node]
                                 for node in one_meaning_data.keys() if node in leaves}
        one_meaning_equalized_leaf_data = {node:one_meaning_equalized_data[node]
                                           for node in one_meaning_equalized_data.keys() if node in leaves}
        num_classes_extended = len(set(map(frozenset, list(one_meaning_data.values()))))
        num_classes_equalized = len(set(one_meaning_equalized_data.values()))
        num_leaf_classes_extended = len(set(map(frozenset, list(one_meaning_leaf_data.values()))))
        num_leaf_classes_equalized = len(set(one_meaning_equalized_leaf_data.values()))
        total_num_classes_extended += num_classes_extended
        total_num_classes_equalized += num_classes_equalized
        total_num_leaf_classes_extended += num_leaf_classes_extended
        total_num_leaf_classes_equalized += num_leaf_classes_equalized
        writer.writerow({"meaning_code": str(m),
                         "max_num_cog_classes": str(num_classes_extended),
                         "min_num_cog_classes": str(num_classes_equalized)})

average_num_classes_extended = total_num_classes_extended / len(list(meanings))
average_num_classes_equalized = total_num_classes_equalized / len(list(meanings))
average_num_leaf_classes_extended = total_num_leaf_classes_extended / len(list(meanings))
average_num_leaf_classes_equalized = total_num_leaf_classes_equalized / len(list(meanings))
print("average num classes extended")
print(average_num_classes_extended)
print("average num classes equalized")
print(average_num_classes_equalized)
print("average num leaf classes extended")
print(average_num_leaf_classes_extended)
print("average num leaf classes equalized")
print(average_num_leaf_classes_equalized)
