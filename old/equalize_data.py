import pickle
# changes the data such that for each meaning, all the languages are considered equal if they share at least one
# cognate class in the original data, and makes the new data set consistent by transitivising the resulting
# equality relations

def equalize_data(cog_data):
    meanings = cog_data.keys()
    equalized_data = dict()
    for m in meanings:
        one_meaning_data = cog_data[m]
        equalized_one_meaning_data = dict()
        lang = one_meaning_data.keys()
        # define equality relation: languages are considered equal if their sets of cognate classes given for
        # the meaning have non-empty intersection
        relation = {(l_1, l_2) for l_1 in lang for l_2 in lang
                     if one_meaning_data[l_1].intersection(one_meaning_data[l_2])}
        # calculate transitive closure of relation
        for i in range(len(lang)):
            for l_1 in lang:
                for l_2 in lang:
                    for l_3 in lang:
                        if (l_1, l_2) in relation and (l_2, l_3) in relation:
                            relation.add((l_1, l_3))
        # partition set of languages by equivalence relation
        partition = [{l_2 for l_2 in lang if (l_1, l_2) in relation} for l_1 in lang]
        partition = map(frozenset, partition)
        partition = list(set(partition))
        # assign new cognate classes which are integer-valued instead of set-valued
        for i in range(len(partition)):
            for lan in partition[i]:
                equalized_one_meaning_data[lan] = i
        equalized_data[m] = equalized_one_meaning_data
    return equalized_data

a_file = open("t1_extended_data.pkl", "rb")
cog_data = pickle.load(a_file)
a_file.close()
equalized_data = equalize_data(cog_data)
a_file = open("t1_equalized_data.pkl", "wb")
pickle.dump(equalized_data, a_file)
a_file.close()
