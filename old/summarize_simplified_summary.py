import sys
sys.path.append("cognate_evolution_models")
import polynesian_trees
import polynesian_data_parameters as pdp
from cognate_evolution_models.lh_calculation import multistate_fix_data_contradictions as mfc
from cognate_evolution_models.small_tools import io_tools as io
from cognate_evolution_models.small_tools import dict_tools as dt
from read_and_truncate_rates import rates_dict
import csv

tree_1 = polynesian_trees.pt_1
tree_2 = polynesian_trees.pt_2
tree_3 = polynesian_trees.pt_3
trees = [tree_1, tree_2, tree_3]

# meanings = rates_dict.keys()

# iso_to_acode = pdp.lang_matches_1
# acode_to_iso = dt.dict_reverse(iso_to_acode)
# acodes_used = acode_to_iso.keys()

for i in {1}:
    tree = trees[i - 1]
    treename = "tree_" + str(i)

    with open("data_summaries/" + treename + "_simplified_summary_summary.csv", 'w', newline='') as csvfile:
        fieldnames = ["times_leaves_consistent", "times_1_leaf_w_duplicates", "times_2_leaves_w_duplicates",
                      "times_3_leaves_w_duplicates",  "times_4_leaves_w_duplicates",  "times_>=5_leaves_w_duplicates",
                      "times_internals_consistent", "times_1_internal_w_duplicates", "times_2_internals_w_duplicates",
                      "times_3_internals_w_duplicates", "times_4_internals_w_duplicates",
                      "times_>=5_internals_w_duplicates",
                      "times_all_data_consistent"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        times_leaves_consistent = 0
        times_1_leaf_w_duplicates = 0
        times_2_leaves_w_duplicates = 0
        times_3_leaves_w_duplicates = 0
        times_4_leaves_w_duplicates = 0
        times_5p_leaves_w_duplicates = 0
        times_internals_consistent = 0
        times_1_internal_w_duplicates = 0
        times_2_internals_w_duplicates = 0
        times_3_internals_w_duplicates = 0
        times_4_internals_w_duplicates = 0
        times_5p_internals_w_duplicates = 0
        times_all_data_consistent = 0
        with open("data_summaries/" + treename + "_simplified_data_summaries.csv", mode='r', encoding="utf8") as inp:
            reader = csv.DictReader(inp)
            for row in reader:
                num_leaves_w_duplicates = int(row["num_leaves_w_duplicates"])
                num_internals_w_duplicates = int(row["num_internals_w_duplicates"])
                if num_leaves_w_duplicates == 0:
                    times_leaves_consistent += 1
                if num_leaves_w_duplicates == 1:
                    times_1_leaf_w_duplicates += 1
                if num_leaves_w_duplicates == 2:
                    times_2_leaves_w_duplicates += 1
                if num_leaves_w_duplicates == 3:
                    times_3_leaves_w_duplicates += 1
                if num_leaves_w_duplicates == 4:
                    times_4_leaves_w_duplicates += 1
                if num_leaves_w_duplicates >= 5:
                    times_5p_leaves_w_duplicates += 1
                if num_internals_w_duplicates == 0:
                    times_internals_consistent += 1
                if num_internals_w_duplicates == 1:
                    times_1_internal_w_duplicates += 1
                if num_internals_w_duplicates == 2:
                    times_2_internals_w_duplicates += 1
                if num_internals_w_duplicates == 3:
                    times_3_internals_w_duplicates += 1
                if num_internals_w_duplicates == 4:
                    times_4_internals_w_duplicates += 1
                if num_internals_w_duplicates >= 5:
                    times_5p_internals_w_duplicates += 1
                if num_leaves_w_duplicates == 0 and num_internals_w_duplicates == 0:
                    times_all_data_consistent += 1

        writer.writerow({"times_leaves_consistent": times_leaves_consistent,
                          "times_1_leaf_w_duplicates": times_1_leaf_w_duplicates,
                          "times_2_leaves_w_duplicates": times_2_leaves_w_duplicates,
                          "times_3_leaves_w_duplicates": times_3_leaves_w_duplicates,
                          "times_4_leaves_w_duplicates": times_4_leaves_w_duplicates,
                          "times_>=5_leaves_w_duplicates": times_5p_leaves_w_duplicates,
                          "times_internals_consistent": times_internals_consistent,
                          "times_1_internal_w_duplicates": times_1_internal_w_duplicates,
                          "times_2_internals_w_duplicates": times_2_internals_w_duplicates,
                          "times_3_internals_w_duplicates": times_3_internals_w_duplicates,
                          "times_4_internals_w_duplicates": times_4_internals_w_duplicates,
                          "times_>=5_internals_w_duplicates": times_5p_internals_w_duplicates,
                          "times_all_data_consistent": times_all_data_consistent})

# io.draw_tree(tree_1)