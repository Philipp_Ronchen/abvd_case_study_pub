import pickle
import copy
a_file = open("t1_extended_data.pkl", "rb")
cog_data = pickle.load(a_file)
a_file.close()

# eliminate all cognate classes for which all languages in which the cognate class appear already have another cognate
# class that they all (and potentially some others) share

meanings = cog_data.keys()

meaning_values = dict()
for m in meanings:
    meaning_values[m] = set()
    for lang in cog_data[m].keys():
        for value in cog_data[m][lang]:
            meaning_values[m].add(value)
    # print(m)
    # print(meaning_values[m])


# value_langs = dict()
# for m in meanings:
#     value_langs[m] = set()
#     for v in meaning_values[m]:
#         value_langs[m][v] = {lang for lang in cog_data[m].keys() if v in cog_data[m][lang]}

new_cog_data = copy.deepcopy(cog_data)

for m in meanings:
    for v1 in meaning_values[m]:
        v1_langs = {lang for lang in new_cog_data[m].keys() if v1 in new_cog_data[m][lang]}
        for v2 in meaning_values[m].difference({v1}):
            v2_langs =  {lang for lang in new_cog_data[m].keys() if v2 in new_cog_data[m][lang]}
            if v2_langs.issubset(v1_langs):
                for lang in new_cog_data[m].keys():
                    if v2 in new_cog_data[m][lang]:
                        new_cog_data[m][lang].remove(v2)

for m in meanings:
    print(m)
    print(cog_data[m])
    print(new_cog_data[m])

a_file = open("t1_simplified_data.pkl", "wb")
pickle.dump(new_cog_data, a_file)
a_file.close()