def parenthetic_contents(string):
    """Generate parenthesized contents in string as pairs (level, contents, start, end), where start is the
    index of the first character within the parentheses, and end is the index of the last character"""
    stack = []
    for i, c in enumerate(string):
        if c == '(':
            stack.append(i)
        elif c == ')' and stack:
            start = stack.pop()
            yield (len(stack), string[start + 1: i], start + 1, i - 1)

print(list(parenthetic_contents('a(b(c(d)e(f)))g')))