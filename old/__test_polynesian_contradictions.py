from ABVD_Data_cleaning import polynesian_trees
from ABVD_Data_cleaning import polynesian_data_parameters as pdp
from lh_calculation import multistate_fix_data_contradictions as mfc
from small_tools import io_tools as io
from small_tools import dict_tools as dt
import csv

tree = polynesian_trees.pt_1

iso_to_acode = pdp.lang_matches_1
acode_to_iso = dt.dict_reverse(iso_to_acode)
acodes_used = acode_to_iso.keys()

meaning_code = 6

one_meaning_data = dict()
with open("ABVD_Data_cleaning/data_meaningwise/" + str(meaning_code) + ".csv", mode='r', encoding="utf8") as inp:
    reader = csv.DictReader(inp)
    for row in reader:
        acode = int(row["LGID"])
        cog_class = row["Cognacy"]
        if acode in acodes_used:
            iso = acode_to_iso[acode]
            dt.dict_add_to_set(one_meaning_data, iso, cog_class)

print("meaning data")
print(one_meaning_data)
extended_data = mfc.extend_set_data(tree, one_meaning_data)
print("extended data")
print(extended_data)

#io.draw_tree(tree)