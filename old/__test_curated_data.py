import sys
sys.path.append("cognate_evolution_models")
import pickle
from polynesian_data_parameters import id_to_word
from polynesian_trees import pt_1
from cognate_evolution_models.lh_calculation.lh_multistate_recursive import extend_data

# read data
a_file = open("t1_extended_data.pkl", "rb")
extended_data = pickle.load(a_file)
a_file.close()

a_file = open("t1_equalized_data.pkl", "rb")
equalized_data = pickle.load(a_file)
a_file.close()

meanings = extended_data.keys()

for m in meanings:
    one_meaning_data = extended_data[m]
    one_meaning_equalized_data = equalized_data[m]
    equalized_data_extended = extend_data(pt_1, one_meaning_equalized_data)[1]
    num_classes_extended = len(set(map(frozenset, list(one_meaning_data.values()))))
    num_classes_equalized = len(set(one_meaning_equalized_data.values()))
    print("Meaning " + str(m) + ": " + str(id_to_word[m]))
    print("Num cog classes: " + str(num_classes_extended) + "/" + str(num_classes_equalized))
    print(one_meaning_data)
    print(one_meaning_equalized_data)
    print(equalized_data_extended == one_meaning_equalized_data)