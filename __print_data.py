import sys
sys.path.append("cognate_evolution_models")
import cognate_evolution_models.small_tools.io_tools as io
from polynesian_trees import pt_1 as tree
import pickle

a_file = open("data_pickled_python/t1_data.pkl", "rb")
data = pickle.load(a_file)
a_file.close()

meanings = data.keys()

for m in meanings:
    print(m)
    print(data[m])

print("num meanings:")
print(len(meanings))

io.draw_tree(tree)