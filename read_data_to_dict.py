import sys
sys.path.append("cognate_evolution_models")
import csv
import pickle
import polynesian_trees as pt
import polynesian_data_parameters as pdp
from cognate_evolution_models.small_tools import dict_tools as dt
from cognate_evolution_models.lh_calculation.multistate_fix_data_contradictions import extend_set_data

a_file = open("data_pickled_python/meanings_used.pkl", "rb")
meanings_used = pickle.load(a_file)
a_file.close()

# reads the data meaningwise, stores it in a dictionary, and extends its to internal that are determined by the
# leaf data.
# Since the data is undecided/setwise (potentially more that one form per meaning) the resulting data set is also
# undecided/setwise

def read_data_to_dict(tree):
    iso_to_acode = {iso:pdp.lang_matches_1[iso] for iso in pdp.lang_matches_1 if iso in tree[0]}
    acode_to_iso = dt.dict_reverse(iso_to_acode)
    acodes_used = acode_to_iso.keys()

    cog_data = dict()
    extended_cog_data = dict()
    for m in sorted(meanings_used):
        one_meaning_data = dict()

        with open("data_meaningwise/" + str(m) + ".csv", mode='r', encoding="utf8") as inp:
            reader = csv.DictReader(inp)
            for row in reader:
                acode = int(row["LGID"])
                cog_class = row["Cognacy"]
                if acode in acodes_used:
                    iso = acode_to_iso[acode]
                    dt.dict_add_to_set(one_meaning_data, iso, cog_class)
        extended_one_meaning_data = extend_set_data(tree, one_meaning_data)
        cog_data[m] = one_meaning_data
        extended_cog_data[m] = extended_one_meaning_data
    return [cog_data, extended_cog_data]


tree_1 = pt.pt_1
data = read_data_to_dict(tree_1)
cog_data = data[0]
extended_cog_data = data[1]
a_file = open("data_pickled_python/t1_data.pkl", "wb")
pickle.dump(cog_data, a_file)
a_file.close()
a_file = open("data_pickled_python/t1_extended_data.pkl", "wb")
pickle.dump(extended_cog_data, a_file)
a_file.close()

