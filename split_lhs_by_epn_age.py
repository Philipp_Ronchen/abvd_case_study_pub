import pickle
import time


def frozendict_to_tuple(fd):
    return((fd["PPn"], fd["PTo"], fd["PNPn"], fd["PEPn"], fd["PCEPn"]))


start_time = time.time()

num_chunks = 20  # number of chunks data was divided into

a_file = open("analysis_results/chunk_0_meanings_lhs_cons_separated_data.pkl", "rb")
some_chunk = pickle.load(a_file)
a_file.close()

all_trees = some_chunk.keys()
possible_ages = {tree["PEPn"] for tree in all_trees}

for age in possible_ages:
    this_age_trees = [frozendict_to_tuple(tree) for tree in all_trees if tree["PEPn"] == age]
    age_data = {tree: dict() for tree in this_age_trees}

    for i in range(num_chunks):
        a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_separated_data.pkl", "rb")
        sep_chunk = pickle.load(a_file)
        a_file.close()
        a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_equalized_data.pkl", "rb")
        eq_chunk = pickle.load(a_file)
        a_file.close()

        sep_chunk = {frozendict_to_tuple(tree): sep_chunk[tree] for tree in sep_chunk.keys()}
        eq_chunk = {frozendict_to_tuple(tree): eq_chunk[tree] for tree in eq_chunk.keys()}

        some_tree = list(sep_chunk.keys())[0]
        chunk_meanings = sep_chunk[some_tree].keys()

        for tree in this_age_trees:
            for m in chunk_meanings:
                age_data[tree][m] = [sep_chunk[tree][m], eq_chunk[tree][m]]

    a_file = open("lhs_by_epn_age/pepn_" + str(age) + "_lhs", "wb")
    pickle.dump(age_data, a_file)
    a_file.close()

end_time = time.time()
elapsed_time = end_time - start_time
print("Time elapsed:")
print(elapsed_time)