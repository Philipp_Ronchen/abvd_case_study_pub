import pickle

a_file = open("data_pickled_python/t1_data.pkl", "rb")
data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_extended_data.pkl", "rb")
extended_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_separated_data.pkl", "rb")
separated_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_equalized_data.pkl", "rb")
equalized_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_extended_separated_data.pkl", "rb")
extended_separated_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_extended_equalized_data.pkl", "rb")
extended_equalized_data = pickle.load(a_file)
a_file.close()

meanings = extended_separated_data.keys()

for m in meanings:
    print(m)
    print(separated_data[m])
    print(equalized_data[m])
    #print(data[m])
    print(extended_separated_data[m])
    print(extended_equalized_data[m])
    #print(extended_data[m])
    # langs_separated = set(extended_separated_data[m].keys())
    # langs_equalized = set(extended_equalized_data[m].keys())
    # langs = langs_separated.union(langs_equalized)
    # for lan in sorted(langs):
    #     print(lan + ":", end="   ")
    #     if lan in extended_separated_data[m]:
    #         print(sorted({lan_2 for lan_2 in langs_separated
    #                       if extended_separated_data[m][lan_2] == extended_separated_data[m][lan]}), end=" / ")
    #     else:
    #         print("n", end=" / ")
    #     if lan in extended_equalized_data[m]:
    #         print(sorted({lan_2 for lan_2 in langs_separated
    #                       if extended_equalized_data[m][lan_2] == extended_equalized_data[m][lan]}))
    #     else:
    #         print("n")
    print("")