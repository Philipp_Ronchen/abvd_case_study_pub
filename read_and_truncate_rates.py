import csv
from gmpy2 import mpfr
import pickle


# reads in the filtered rates and truncates them, that is changing change rates >= 96 to 96 and change rates >= 4
# to 4.

rates_dict = dict()
mpfr_rates_dict = dict()
with open('data_cleaning/filtered_rates.csv', mode='r', encoding="utf8") as inp:
    reader = csv.DictReader(inp)
    for row in reader:
        word_id = int(row["word_id"])
        ret_rate = int(row["ret_rate"])
        if ret_rate >= 96:
            ret_rate = 96
        elif ret_rate <= 4:
            ret_rate = 4
        mpfr_ret_rate = mpfr(ret_rate)
        rates_dict[word_id] = ret_rate
        mpfr_rates_dict[word_id] = mpfr_ret_rate

a_file = open("data_pickled_python/rates_dict.pkl", "wb")
pickle.dump(rates_dict, a_file)
a_file.close()

a_file = open("data_pickled_python/mpfr_rates_dict.pkl", "wb")
pickle.dump(mpfr_rates_dict, a_file)
a_file.close()

print(rates_dict)
print(mpfr_rates_dict)