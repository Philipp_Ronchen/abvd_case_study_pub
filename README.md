# General information

This repository contains all the material needed to replicate the Case study from the article Calculating 
*Likelihood calculation in a multistate model of vocabulary
evolution for linguistic dating*. More details can be found in the article and its appendix.

This repository contains the repository "cognate_evolution_models" as a submodule. The submodule contains the core 
likelihood and tree algorithms.

The main data used for this case study was extracted from the Austronesian Basic Vocabulary Database 
<https://abvd.shh.mpg.de/austronesian/>, see the webpage for license information.

# Replicating the main study
Order the different scripts should be executed to replicate study,
starting with the files abvd.tab, iso_codes.txt and retention_rates.csv
in data_cleaning.
1. `data_cleaning/filter_retention_rates.R`
2. `data_cleaning/filter_abvd_to_pn.R`
3. `data_cleaning/filter_abvd_by_coding.R`
4. `data_cleaning/divide_meaningwise`
5. `read_and_truncate_rates.py`
6. `get_meanings_used.py`
7. `read_data_to_dict.py`
8. `separata_data.py`
9. `equalize_data.py`
10. `consistify_data.py`
11. `get_model_change_rates.py`
12. Run the function `ex` from `calculate_lhs` once with parameter "sep"
and once with the parameter "eq".
13. `split_lhs_by_tree_age.py`
14. `split_lhs_by_epn_age.py`
15. `get_max_lhs_by_tree_or_epn_age.py`

Step 12.-14. are time- and memory-intensive.

# Replicating the control study

To replicate the meaningswise Stochastic Dollo control study (after replicating the main study), 
execute the following scripts (all steps are time- and memory-intesive):
1. Run the function `ex` from `sdm_calculate_lhs` once with parameter "first_chunks"
and once with the parameter "second_chunks".
2. `sdm_split_lhs_by_tree_age.py`
3. `sdm_split_lhs_by_epn_age.py`
4. `sdm_get_max_lhs_by_tree_or_epn_age.py`

# Where to find results

The results of both the main and the control study can be found in the directory `max_lhs_summaries`. 
Intermediate results (which would have been found `analysis_results`, `lhs_by_tree_age` and `lhs_by_epn_age`) are not stored in this repository because of their size (tens of GBs).