import sys
sys.path.append("cognate_evolution_models")
import polynesian_trees
import polynesian_data_parameters as pdp
from cognate_evolution_models.lh_calculation import multistate_fix_data_contradictions as mfc
from cognate_evolution_models.small_tools import io_tools as io
from cognate_evolution_models.small_tools import dict_tools as dt
#from get_filtered_rates import rates_dict
import csv
import pickle

tree_1 = polynesian_trees.pt_1
tree_2 = polynesian_trees.pt_2
tree_3 = polynesian_trees.pt_3
trees = [tree_1, tree_2, tree_3]

# read data
a_file = open("data_pickled_python/t1_extended_data.pkl", "rb")
extended_data = pickle.load(a_file)
a_file.close()


meanings = extended_data.keys()

# iso_to_acode = pdp.lang_matches_1
# acode_to_iso = dt.dict_reverse(iso_to_acode)
# acodes_used = acode_to_iso.keys()

for i in {1}:
    tree = trees[i-1]
    treename = "tree_" + str(i)

    with open("data_summaries/" + treename + "_summaries.csv", 'w', newline='') as csvfile:
        fieldnames = ["meaning_code", "num_leaves_consistent", "num_partitions_among_leaves_w_1",
                      "num_leaves_w_duplicates", "num_leaves_w_0", "num_leaves_w_1", "num_leaves_w_2",
                      "num_leaves_w_3", "num_leaves_w_4", "num_leaves_w_>=5",
                      "num_internals_consistent", "num_internals_w_duplicates",
                      "num_internals_w_0", "num_internals_w_1", "num_internals_w_2", "num_internals_w_3",
                      "num_internals_w_4",  "num_internals_w_>=5"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for m in sorted(meanings):

            one_meaning_data = extended_data[m]
            # with open("data_meaningwise/" + str(m) + ".csv", mode='r', encoding="utf8") as inp:
            #     reader = csv.DictReader(inp)
            #     for row in reader:
            #         acode = int(row["LGID"])
            #         cog_class = row["Cognacy"]
            #         if acode in acodes_used:
            #             iso = acode_to_iso[acode]
            #             dt.dict_add_to_set(one_meaning_data, iso, cog_class)
            # extended_data = mfc.extend_set_data(tree, one_meaning_data)
            leaves = tree[3]
            internals = tree[4]
            internals_data = {node:one_meaning_data[node] for node in one_meaning_data if node in internals}
            leaves_w_1 = {node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) == 1}
            # partitions_among_leaves_w_1 = \
            # {frozenset({node for node in consistent_leaves if one_meaning_data[node] == one_meaning_data[current_node]})
            #  for current_node in consistent_leaves}
            # print(partitions_among_leaves_w_1)
            consistent_leaf_data = {node:list(one_meaning_data[node])[0] for node in leaves_w_1 }
            num_leaves_consistent = len({node for node in leaves if node not in one_meaning_data or (node in one_meaning_data and len(one_meaning_data[node]) == 1)})
            num_partitions_among_leaves_w_1 = len(set(consistent_leaf_data.values()))
            num_leaves_w_duplicates = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) >= 2})
            num_leaves_w_0 = len({node for node in leaves if node in one_meaning_data and node not in one_meaning_data == 0})
            num_leaves_w_1 = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) == 1})
            num_leaves_w_2 = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) == 2})
            num_leaves_w_3 = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) == 3})
            num_leaves_w_4 = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) == 4})
            num_leaves_w_5p = len({node for node in leaves if node in one_meaning_data and len(one_meaning_data[node]) >= 5})
            num_internals_consistent = len({node for node in internals if node not in one_meaning_data or (node in one_meaning_data and len(one_meaning_data[node]) == 1)})
            num_internals_w_duplicates = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) >= 2})
            num_internals_w_0 = len({node for node in internals if node not in one_meaning_data})
            num_internals_w_1 = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) == 1})
            num_internals_w_2 = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) == 2})
            num_internals_w_3 = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) == 3})
            num_internals_w_4 = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) == 4})
            num_internals_w_5p = len({node for node in internals if node in one_meaning_data and len(one_meaning_data[node]) >= 5})

            writer.writerow({"meaning_code": str(m),
                             "num_leaves_consistent": str(num_leaves_consistent),
                             "num_partitions_among_leaves_w_1": str(num_partitions_among_leaves_w_1),
                             "num_leaves_w_duplicates": str(num_leaves_w_duplicates),
                             "num_leaves_w_0": str(num_leaves_w_0),
                             "num_leaves_w_1": str(num_leaves_w_1),
                             "num_leaves_w_2": str(num_leaves_w_2),
                             "num_leaves_w_3": str(num_leaves_w_3),
                             "num_leaves_w_4": str(num_leaves_w_4),
                             "num_leaves_w_>=5": str(num_leaves_w_5p),
                             "num_internals_consistent": str(num_internals_consistent),
                             "num_internals_w_duplicates": str(num_internals_w_duplicates),
                             "num_internals_w_0": str(num_internals_w_0),
                             "num_internals_w_1": str(num_internals_w_1),
                             "num_internals_w_2": str(num_internals_w_2),
                             "num_internals_w_3": str(num_internals_w_3),
                             "num_internals_w_4": str(num_internals_w_4),
                             "num_internals_w_>=5": str(num_internals_w_5p)})
        


#io.draw_tree(tree_1)