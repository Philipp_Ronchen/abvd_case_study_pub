import sys
sys.path.append("cognate_evolution_models")
import pickle
import csv
from cognate_evolution_models.small_tools import math_tools

# extracts the maximum likelihoods per tree age given the different alternative data sets, for different truncation
# levels, that is a different number of meanings excluded. Writes both a python dictionary and a csv.


ages = list(range(600,3501,100))

truncation_levels = [0, 5, 10, 15, 20, 30, 40, 50]

for param in ["by_tree_age", "by_epn_age"]:
    for truncation_level in truncation_levels:
        if param == "by_tree_age":
            csv_filepath = "max_lhs_summaries/sdm_max_lhs_summary_by_ppn_age_trunc_" + str(truncation_level) + ".csv"
        else:
            csv_filepath = "max_lhs_summaries/sdm_max_lhs_summary_by_pepn_age_trunc_" + str(truncation_level) + ".csv"
        with open(csv_filepath, 'w', newline='') as csvfile:
            fieldnames = ["age", "max_lh",
                          "tree_max_lh",
                          "meanings_excluded"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            for age in ages:
                if param == "by_tree_age":
                    a_file = open("lhs_by_tree_age/sdm_ppn_" + str(age) + "_lhs", "rb")
                else:
                    a_file = open("lhs_by_epn_age/sdm_pepn_" + str(age) + "_lhs", "rb")
                lhs = pickle.load(a_file)
                a_file.close()

                some_tree = list(lhs.keys())[0]
                meanings = lhs[some_tree].keys()

                # per tree, for the given truncation level, stores the sum of logs of the lhs, truncating the
                # smallest lhs
                lhs_log_sums = dict()

                # per tree, for the given truncation level, stores which meanings are excluded when maximising the product
                # of the highest likelihoods meaningswise
                meanings_excluded_per_tree = dict()


                for tree in lhs.keys():
                    lh_items = list(lhs[tree].items())
                    lh_items.sort(key=lambda x: x[1], reverse=True)

                    if truncation_level == 0:
                        trunc_lh_items = lh_items
                    else:
                        trunc_lh_items = lh_items[:-truncation_level]
                    meanings_left = {item[0] for item in trunc_lh_items}
                    meanings_excluded = {m for m in meanings if m not in meanings_left}

                    trunc_log_lhs = [math_tools.inf_log(item[1]) for item in trunc_lh_items]

                    lh_log_sum = sum(trunc_log_lhs)

                    meanings_excluded_per_tree[tree] = meanings_excluded
                    lhs_log_sums[tree] = lh_log_sum

                tree_lh_items = list(lhs_log_sums.items())
                tree_lh_items.sort(key=lambda x: x[1], reverse=True)

                (tree_max, max_lh) = tree_lh_items[0]

                meanings_excluded_max_lh = meanings_excluded_per_tree[tree_max]


                writer.writerow({"age": str(age),
                                 "max_lh": str(max_lh),
                                 "tree_max_lh": str(tree_max),
                                 "meanings_excluded": str(meanings_excluded_max_lh),
                                 })
