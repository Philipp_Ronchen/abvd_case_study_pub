import sys
sys.path.append("cognate_evolution_models")
from cognate_evolution_models.small_tools.list_tools import chunk_list
import cognate_evolution_models.data_transform.multistate_data_transf as mdt
import cognate_evolution_models.branch_length_tree_operations.meaning_lhs as ml
from polynesian_trees import pt_1 as tree_topology
from polynesian_trees import pt_1_ac as age_constraints
import time
import pickle


# Calculates for all trees with the given tree_topology and compatible with age_constraints and the specified max_height
# and age_resolution all likelihoods meaningswise, according to the meaningswise Stochastic Dollo model (SDM).
# Death rates are the same as the change rates for the Multistate model for the main analysis, birth rates are chosen
# such that the observed average number of traits per language in the data (for a given meaning) matches the equilibrium
# distribution of the SD process
# param is either "first_chunks" or "second_chunks"

def ex(param):
    start_time = time.time()
    num_chunks = 60

    max_tree_height = 3500
    age_resolution = 100

    a_file = open("data_pickled_python/t1_data.pkl", "rb")
    multistate_data = pickle.load(a_file)
    a_file.close()

    a_file = open("data_pickled_python/meanings_used.pkl", "rb")
    meanings = pickle.load(a_file)
    a_file.close()

    a_file = open("data_pickled_python/model_change_rates.pkl", "rb")
    multistate_change_rates = pickle.load(a_file)
    a_file.close()

    meanings_list = sorted(list(meanings))
    meaning_chunks = chunk_list(meanings_list, num_chunks)

    chunk_indices = list(range(num_chunks))
    if param == "first_chunks":
        chunks_to_compute = chunk_indices[:30]
    elif param == "second_chunks":
        chunks_to_compute = chunk_indices[30:]
    else:
        raise ValueError("param has to be 'first_chunks' or 'second_chunks'")

    for i in chunks_to_compute:
        print("num meaning chunk")
        print(i)
        m_chunk = meaning_chunks[i]
        print("meaning chunk")
        print(m_chunk)
        multistate_data_chunk = {m:multistate_data[m] for m in m_chunk}
        multistate_change_rates_chunk = {m:multistate_change_rates[m] for m in m_chunk}

        sdm_packaged_data = mdt.multistate_setwise_to_sdm_estimate_eq(multistate_change_rates_chunk,
                                                                      multistate_data_chunk)
        print("sdm packaged data")
        print(sdm_packaged_data)

        meaning_lhs = ml.sdm_meaningwise(tree_topology, max_tree_height, age_constraints, age_resolution,
                                         sdm_packaged_data)

        a_file = open("analysis_results/sdm_chunk_" + str(i) + "_meanings_lhs.pkl", "wb")
        pickle.dump(meaning_lhs, a_file)
        a_file.close()

    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Time elapsed:")
    print(elapsed_time)
    return None




