import sys
sys.path.append("cognate_evolution_models")
from cognate_evolution_models.lh_calculation.lh_multistate_recursive import extend_data
import polynesian_trees
import pickle
# changes the data such that for each meaning, all the languages are considered to have equal data
# if and only if they have exactly the same set of cognates in the original (setwise) data,
# The cognate data which is given as input should be non-extended

tree_1 = polynesian_trees.pt_1

def separate_data(cog_data):
    meanings = cog_data.keys()
    separated_data = dict()
    extended_separated_data = dict()
    for m in meanings:
        print(m)
        one_meaning_data = cog_data[m]
        separated_one_meaning_data = dict()
        lang = one_meaning_data.keys()
        # define equality relation: languages are considered equal if their sets of cognate classes given for
        # the meaning have non-empty intersection
        relation = {(l_1, l_2) for l_1 in lang for l_2 in lang
                     if one_meaning_data[l_1] == one_meaning_data[l_2]}
        # partition set of languages by equivalence relation
        partition = [{l_2 for l_2 in lang if (l_1, l_2) in relation} for l_1 in lang]
        partition = map(frozenset, partition)
        partition = list(set(partition))
        # assign new cognate classes which are integer-valued instead of set-valued
        for i in range(len(partition)):
            for lan in partition[i]:
                separated_one_meaning_data[lan] = i
        extension = extend_data(tree_1, separated_one_meaning_data)
        is_consistent = extension[0]
        if is_consistent:
            # make data setwise for comparisons with meanings where the extended data is inconsistent
            extended_separated_one_meaning_data = {node: {extension[1][node]} for node in extension[1]}
        else:
            extended_separated_one_meaning_data = extension[1]
        separated_data[m] = separated_one_meaning_data
        extended_separated_data[m] = extended_separated_one_meaning_data
    return [separated_data, extended_separated_data, is_consistent]


a_file = open("data_pickled_python/t1_data.pkl", "rb")
cog_data = pickle.load(a_file)
a_file.close()
result_data = separate_data(cog_data)
separated_data = result_data[0]
extended_separated_data = result_data[1]
a_file = open("data_pickled_python/t1_separated_data.pkl", "wb")
pickle.dump(separated_data, a_file)
a_file.close()
a_file = open("data_pickled_python/t1_extended_separated_data.pkl", "wb")
pickle.dump(extended_separated_data, a_file)
a_file.close()