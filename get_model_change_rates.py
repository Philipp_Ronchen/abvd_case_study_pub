import sys
sys.path.append("cognate_evolution_models")
from cognate_evolution_models.data_transform import change_rate_transf as crt
from cognate_evolution_models.data_transform import gmp2_change_rate_transf as gmp2_crt
import pickle

a_file = open("data_pickled_python/meanings_used.pkl", "rb")
meanings = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/rates_dict.pkl", "rb")
rates_dict = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/mpfr_rates_dict.pkl", "rb")
mpfr_rates_dict = pickle.load(a_file)
a_file.close()

model_change_rates = {m: crt.ret_millenium_to_multistate_cr(0.01*rates_dict[m]) for m in meanings}

mpfr_model_change_rates = {m: gmp2_crt.ret_millenium_to_multistate_cr(0.01*mpfr_rates_dict[m]) for m in meanings}

a_file = open("data_pickled_python/model_change_rates.pkl", "wb")
pickle.dump(model_change_rates, a_file)
a_file.close()

a_file = open("data_pickled_python/mpfr_model_change_rates.pkl", "wb")
pickle.dump(mpfr_model_change_rates, a_file)
a_file.close()

print(model_change_rates)
print(mpfr_model_change_rates)
