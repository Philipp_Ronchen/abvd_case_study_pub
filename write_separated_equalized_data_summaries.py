import pickle
import polynesian_trees as pt
import csv

# read data
a_file = open("data_pickled_python/t1_separated_data.pkl", "rb")
separated_data = pickle.load(a_file)
a_file.close()

a_file = open("data_pickled_python/t1_equalized_data.pkl", "rb")
equalized_data = pickle.load(a_file)
a_file.close()

# a_file = open("t1_extended_separated_data.pkl", "rb")
# extended_separated_data = pickle.load(a_file)
# a_file.close()
#
# a_file = open("t1_extended_equalized_data.pkl", "rb")
# extended_equalized_data = pickle.load(a_file)
# a_file.close()

meanings = separated_data.keys()

leaves = pt.pt_1[3]

treename = "tree_1"

total_num_classes_separated = 0
total_num_classes_equalized = 0
with open("data_summaries/" + treename + "_separated_equalized_data_summaries.csv", 'w', newline='') as csvfile:
    fieldnames = ["meaning_code", "separated_num_cog_classes", "equalized_num_cog_classes"]#,
                  #"sep_num_internals_contradictory", "eq_num_internals_contradictory"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for m in meanings:
        one_meaning_separated_data = separated_data[m]
        one_meaning_equalized_data = equalized_data[m]
        one_meaning_separated_leaf_data = {node:one_meaning_separated_data[node]
                                 for node in one_meaning_separated_data.keys() if node in leaves}
        one_meaning_equalized_leaf_data = {node:one_meaning_equalized_data[node]
                                           for node in one_meaning_equalized_data.keys() if node in leaves}
        num_classes_separated = len(set(one_meaning_separated_leaf_data.values()))
        num_classes_equalized = len(set(one_meaning_equalized_leaf_data.values()))
        total_num_classes_separated += num_classes_separated
        total_num_classes_equalized += num_classes_equalized
        writer.writerow({"meaning_code": str(m),
                         "separated_num_cog_classes": str(num_classes_separated),
                         "equalized_num_cog_classes": str(num_classes_equalized)})

average_num_classes_extended = total_num_classes_separated / len(list(meanings))
average_num_classes_equalized = total_num_classes_equalized / len(list(meanings))
print("average num classes extended")
print(average_num_classes_extended)
print("average num classes equalized")
print(average_num_classes_equalized)
