import sys
sys.path.append("cognate_evolution_models")
from cognate_evolution_models.small_tools.list_tools import chunk_list
import cognate_evolution_models.data_transform.multistate_data_transf as mdt
import cognate_evolution_models.branch_length_tree_operations.meaning_lhs as ml
from polynesian_trees import pt_1 as tree_topology
from polynesian_trees import pt_1_ac as age_constraints
import time
import pickle

# Calculates for all trees with the given tree_topology and compatible with age_constraints and the specified max_height
# and age_resolution all likelihoods meaningswise. Either for data set t1_cons_separated_data
# or for the data set t1_cons_equalized_data. Specified by the parameter which can take the values "sep" or "eq"

def ex(param):
    start_time = time.time()
    num_chunks = 20

    max_tree_height = 3500
    age_resolution = 100

    if param == "sep":
        a_file = open("data_pickled_python/t1_cons_separated_data.pkl", "rb")
        data_set = pickle.load(a_file)
        a_file.close()
    elif param == "eq":
        a_file = open("data_pickled_python/t1_cons_equalized_data.pkl", "rb")
        data_set = pickle.load(a_file)
        a_file.close()
    else:
        raise ValueError("Parameter given has to be 'sep' or 'eq'")
    a_file = open("data_pickled_python/meanings_used.pkl", "rb")
    meanings = pickle.load(a_file)
    a_file.close()

    a_file = open("data_pickled_python/model_change_rates.pkl", "rb")
    change_rates = pickle.load(a_file)
    a_file.close()

    meanings_list = sorted(list(meanings))
    meaning_chunks = chunk_list(meanings_list, num_chunks)

    for i in range(num_chunks):
        m_chunk = meaning_chunks[i]
        data_chunk = {m:data_set[m] for m in m_chunk}
        change_rates_chunk = {m:change_rates[m] for m in m_chunk}

        packaged_data = mdt.package_multiple_data(change_rates_chunk, data_chunk, tree_topology)

        meaning_lhs = ml.multiple_data_multistate_meaningwise(tree_topology, max_tree_height, age_constraints,
                                                          age_resolution, packaged_data)

        if param == "sep":
            a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_separated_data.pkl", "wb")
        elif param == "eq":
            a_file = open("analysis_results/chunk_" + str(i) + "_meanings_lhs_cons_equalized_data.pkl", "wb")
        pickle.dump(meaning_lhs, a_file)
        a_file.close()

        print("num meaning chunk")
        print(i)
        print("meaning chunk")
        print(m_chunk)

    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Time elapsed:")
    print(elapsed_time)
    return None



