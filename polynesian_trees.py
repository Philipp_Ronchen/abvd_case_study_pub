import sys
sys.path.append("cognate_evolution_models")
from cognate_evolution_models.trees import tree_basics

# tree topologies that can be used in the study. These are subtrees of the Glottolog tree for Polynesian, chosen
# in a way such that
# - all the language varieties in the tree appear in ABVD and have good coverage
# - there are few internal nodes to facilitate fast likelihood calculations when iterating through many possible
# branch lengths
# pt_2 is pt_1 with one leaf and one internal added. pt_3 is pt_2 with one leaf and one internal added

max_tree_height = 3500

# Polynesian tree 1, very small
nodes = {"PPn", "PTo", "PNPn", "PEPn", "PCEPn",
         "niu", "ton", "fud", "pkp", "mnv", "tkp", "wls", "smo", "fut",
         "rap", "haw", "mrv", "mri", "pmt", "tah", "mrq", "rar"}
edges = {("PPn", "PTo"): 1, ("PPn", "PNPn"): 1, ("PNPn", "PEPn"): 1, ("PEPn","PCEPn"): 1,
         ("PTo", "niu"): 1, ("PTo", "ton"): 1, ("PNPn", "fud"): 1, ("PNPn", "pkp"): 1, ("PNPn", "mnv"): 1,
         ("PNPn", "tkp"): 1, ("PNPn", "wls"): 1, ("PNPn", "smo"): 1, ("PNPn", "fut"): 1,
         ("PEPn", "rap"): 1, ("PCEPn", "haw"): 1, ("PCEPn", "mrv"): 1, ("PCEPn", "mri"): 1,
         ("PCEPn", "pmt"): 1, ("PCEPn", "tah"): 1, ("PCEPn", "mrq"): 1, ("PCEPn", "rar"): 1}
root = "PPn"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

pt_1 = [nodes, edges, root, leaves, internals, parents, children]

pt_1_ac = {"PPn": [600, 3500], "PTo": [0, 3500], "PNPn": [600, 3500], "PEPn": [600, 3500], "PCEPn": [600, 3500],
           "niu": [0, 0], "ton": [0, 0], "fud": [0, 0], "pkp": [0, 0], "mnv": [0, 0], "tkp": [0, 0], "wls": [0, 0],
           "smo": [0, 0], "fut": [0, 0], "rap": [0, 0], "haw": [0, 0], "mrv": [0, 0], "mri": [0, 0], "pmt": [0, 0],
           "tah": [0, 0], "mrq": [0, 0], "rar": [0, 0]}


# Polynesian tree 2, add Northern Outlier Polyynesian-East Polynesian
nodes = {"PPn", "PTo", "PNPn", "PNOPn-EPn", "PEPn", "PCEPn",
         "niu", "ton", "fud", "pkp", "mnv", "tkp", "wls", "smo", "fut",
         "kpg", "rap", "haw", "mrv", "mri", "pmt", "tah", "mrq", "rar"}
edges = {("PPn", "PTo"): 1, ("PPn", "PNPn"): 1, ("PNPn", "PNOPn-EPn"): 1, ("PNOPn-EPn", "PEPn"): 1, ("PEPn","PCEPn"): 1,
         ("PTo", "niu"): 1, ("PTo", "ton"): 1, ("PNPn", "fud"): 1, ("PNPn", "pkp"): 1, ("PNPn", "mnv"): 1,
         ("PNPn", "tkp"): 1, ("PNPn", "wls"): 1, ("PNPn", "smo"): 1, ("PNPn", "fut"): 1,
         ("PNOPn-EPn", "kpg"): 1, ("PEPn", "rap"): 1, ("PCEPn", "haw"): 1, ("PCEPn", "mrv"): 1, ("PCEPn", "mri"): 1,
         ("PCEPn", "pmt"): 1, ("PCEPn", "tah"): 1, ("PCEPn", "mrq"): 1, ("PCEPn", "rar"): 1}
root = "PPn"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

pt_2 = [nodes, edges, root, leaves, internals, parents, children]

pt_2_ac = {"PPn": [600, 3500], "PTo": [0, 3500], "PNPn": [600, 3500], "PNOPn-EPn": [600, 3500],
           "PEPn": [600, 3500], "PCEPn": [600, 3500],
           "niu": [0, 0], "ton": [0, 0], "fud": [0, 0], "pkp": [0, 0], "mnv": [0, 0], "tkp": [0, 0], "wls": [0, 0],
           "smo": [0, 0], "fut": [0, 0], "kpg": [0, 0], "rap": [0, 0], "haw": [0, 0], "mrv": [0, 0], "mri": [0, 0],
           "pmt": [0, 0], "tah": [0, 0], "mrq": [0, 0], "rar": [0, 0]}


# Polynesian tree 3, add Solomons-Northern Outlier-East Polynesian
nodes = {"PPn", "PTo", "PNPn", "PNOPn-EPn", "PSNO-EPn", "PEPn", "PCEPn",
         "niu", "ton", "fud", "pkp", "mnv", "tkp", "wls", "smo", "fut",
         "kpg", "sky", "rap", "haw", "mrv", "mri", "pmt", "tah", "mrq", "rar"}
edges = {("PPn", "PTo"): 1, ("PPn", "PNPn"): 1, ("PNPn", "PNOPn-EPn"): 1, ("PNOPn-EPn", "PSNO-EPn"): 1,
         ("PSNO-EPn", "PEPn"): 1, ("PEPn","PCEPn"): 1,
         ("PTo", "niu"): 1, ("PTo", "ton"): 1, ("PNPn", "fud"): 1, ("PNPn", "pkp"): 1, ("PNPn", "mnv"): 1,
         ("PNPn", "tkp"): 1, ("PNPn", "wls"): 1, ("PNPn", "smo"): 1, ("PNPn", "fut"): 1,
         ("PNOPn-EPn", "kpg"): 1, ("PSNO-EPn", "sky"): 1, ("PEPn", "rap"): 1,
         ("PCEPn", "haw"): 1, ("PCEPn", "mrv"): 1, ("PCEPn", "mri"): 1, ("PCEPn", "pmt"): 1, ("PCEPn", "tah"): 1,
         ("PCEPn", "mrq"): 1, ("PCEPn", "rar"): 1}
root = "PPn"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

pt_3 = [nodes, edges, root, leaves, internals, parents, children]

pt_3_ac = {"PPn": [600, 3500], "PTo": [0, 3500], "PNPn": [600, 3500], "PSNO-EPn": [600, 3500],
           "PNOPn-EPn": [600, 3500], "PEPn": [600, 3500], "PCEPn": [600, 3500],
           "niu": [0, 0], "ton": [0, 0], "fud": [0, 0], "pkp": [0, 0], "mnv": [0, 0], "tkp": [0, 0], "wls": [0, 0],
           "smo": [0, 0], "fut": [0, 0], "kpg": [0, 0], "sky": [0, 0], "rap": [0, 0], "haw": [0, 0], "mrv": [0, 0],
           "mri": [0, 0], "pmt": [0, 0], "tah": [0, 0], "mrq": [0, 0], "rar": [0, 0]}