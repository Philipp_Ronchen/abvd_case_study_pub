# creates a file containing the iso3-codes of all the languages classified as "Polynesian" in Glottologue
from pyglottolog import Glottolog

glottolog = Glottolog('C:/Users/hp840/Documents/_Uni/Verktyg/glottolog-4.3')
languoids = {l.id: l for l in glottolog.languoids()}

polynesian = glottolog.languoid("poly1242")
descendants = polynesian.descendants_from_nodemap(languoids)
language_descendants = [d for d in descendants if d.category == "Spoken L1 Language"]
iso_codes = list(map(lambda x:x.iso_code, language_descendants))
file = open("iso_codes.txt", "w+")
for code in iso_codes[:-1]:
    file.write(code + "\n")
file.write(iso_codes[-1])
file.close()