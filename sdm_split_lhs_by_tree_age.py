import pickle
import time


def frozendict_to_tuple(fd):
    return((fd["PPn"], fd["PTo"], fd["PNPn"], fd["PEPn"], fd["PCEPn"]))


start_time = time.time()

num_chunks = 60 # number of chunks data was divided into

a_file = open("analysis_results/sdm_chunk_0_meanings_lhs.pkl", "rb")
some_chunk = pickle.load(a_file)
a_file.close()

all_trees = some_chunk.keys()
possible_ages = {tree["PPn"] for tree in all_trees}

for age in possible_ages:
    this_age_trees = [frozendict_to_tuple(tree) for tree in all_trees if tree["PPn"] == age]
    age_data = {tree: dict() for tree in this_age_trees}

    for i in range(num_chunks):
        a_file = open("analysis_results/sdm_chunk_" + str(i) + "_meanings_lhs.pkl", "rb")
        chunk = pickle.load(a_file)
        a_file.close()

        chunk = {frozendict_to_tuple(tree): chunk[tree] for tree in chunk.keys()}

        some_tree = list(chunk.keys())[0]
        chunk_meanings = chunk[some_tree].keys()

        for tree in this_age_trees:
            for m in chunk_meanings:
                age_data[tree][m] = chunk[tree][m]

    a_file = open("lhs_by_tree_age/sdm_ppn_" + str(age) + "_lhs", "wb")
    pickle.dump(age_data, a_file)
    a_file.close()


end_time = time.time()
elapsed_time = end_time - start_time
print("Time elapsed:")
print(elapsed_time)
